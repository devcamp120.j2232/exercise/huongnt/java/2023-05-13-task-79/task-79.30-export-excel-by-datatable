package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.repository.*;
import com.devcamp.pizza365.service.ExcelExporter2;

import java.util.*;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class Customer2Controller {
	@Autowired
	Customer2Repository Customer2Rep;
	@Autowired
	Order2Repository orderRep;
	@Autowired
	OrderDetailRepository orderDetailRep;

	@GetMapping("/allCustomers2")
	public ResponseEntity<Object> getAllCustomer2s() {
		try {
			List<Customer2> Customer2 = new ArrayList<Customer2>();

			Customer2Rep.findAll().forEach(Customer2::add);

			return new ResponseEntity<>(Customer2, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/allOrders2")
	public ResponseEntity<Object> getAllOrders() {
		try {
			List<Order2> orders = new ArrayList<Order2>();

			orderRep.findAll().forEach(orders::add);

			return new ResponseEntity<>(orders, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			// return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



	@GetMapping("/export/Customers/excel2")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<Customer2> Customer2 = new ArrayList<Customer2>();

		Customer2Rep.findAll().forEach(Customer2::add);

		ExcelExporter2 excelExporter = new ExcelExporter2(Customer2);

		excelExporter.export(response);
	}
}
